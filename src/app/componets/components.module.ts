import { ReactiveFormsModule } from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";
import { SppinerComponent } from "./ui/loadings/sppiner/sppiner.component";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NavbarComponent } from './ui/navbar/navbar.component';
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

@NgModule({
    declarations: [
        SppinerComponent,
        NavbarComponent
    ],
    exports: [
      ReactiveFormsModule,
      SppinerComponent,
      NavbarComponent
    ],
    imports: [
        NgxSpinnerModule,
        RouterModule,
        CommonModule
    ],  
    schemas: [
      CUSTOM_ELEMENTS_SCHEMA
    ],
    providers: [
    ]
  })
  export class ComponentsModule { }
  