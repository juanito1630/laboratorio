
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL } from 'src/app/conf/conf';

@Injectable({
  providedIn: 'root'
})
export class NuevosEstudiosService {
  public url = URL;

  constructor( private _http: HttpClient ) { }

   // ? Post de nuevos Estudios
   enviarEstudiosNuevos( data: any){
    const uri = `${this.url}/api/register/product`;
    return this._http.post( uri, data  );
  }


  
  // ? Get de los estudios
  getEstudios() {
    const uri = `${this.url}/api/list/product`;
    return this._http.get(uri)

  }
  

}
