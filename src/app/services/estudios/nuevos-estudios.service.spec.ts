import { TestBed } from '@angular/core/testing';

import { NuevosEstudiosService } from './nuevos-estudios.service';

describe('NuevosEstudiosService', () => {
  let service: NuevosEstudiosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NuevosEstudiosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
