import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL } from 'src/app/conf/conf';


@Injectable({
  providedIn: 'root'
})
export class PacientesService {
  public url = URL;


  constructor(private _http: HttpClient) { }


  // ? Post del Paciente
  setPacientes( data: any){
    const uri = `${this.url}/api/register/pacientes`;
    return this._http.post( uri, data  );
  }

  
  // ? Get de los Pacientes
  getPacientes() {
    const uri = `${this.url}/api/list/pacientes`;
    return this._http.get(uri)

  }
}
