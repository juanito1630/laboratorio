import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

// UI 

import { NgxSpinnerService} from 'ngx-spinner'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private spinner:NgxSpinnerService
  ) { }

  ngOnInit(): void {
  }


  registrar(f: NgForm){

    //validamos el formulario
    if( f.valid  ){

      //obtenemos los valores de los controles
      const password =  f.form.controls.password.value;
      const usuario =  f.form.controls.usuario.value;

      // hacemos el trim para quitar espacios
      password.trim();
      usuario.trim();
      this.spinner.show();
      const requestToLogin = {
        password,
        usuario
      };

      console.log(requestToLogin);
    }else {
      Swal.fire('COMPLETA LOS DATOS', 'Revisa los datos', 'error');
    }

  }

}
