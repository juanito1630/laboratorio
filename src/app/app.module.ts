import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//* Paginador
import { NgxPaginationModule } from 'ngx-pagination';

// imports 
import { AppComponent } from './app.component';
import { LoginModule } from './login/login/login.module';
import { HttpClientModule  } from '@angular/common/http';
import { APP_ROUTES } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageModule } from './pages/page.module';
import { ComponentsModule } from './componets/components.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  exports: [
    ReactiveFormsModule
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserModule,
    APP_ROUTES,
    ReactiveFormsModule,
    FormsModule,
    LoginModule,
    PageModule,
    ComponentsModule,
    NgxPaginationModule
  ],  
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
