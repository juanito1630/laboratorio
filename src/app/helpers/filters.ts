
// recibe el arreglo al que se le hara la busqueda y termino de busqueda
export function buscarPaciente( pacientes:any[] , busqueda:any ) {
    const pacientesFilter:any[] = [];
    pacientes.forEach((element:any) => {
        if(element.ESTUDIO.includes(busqueda.toUpperCase())){
            pacientesFilter.push(element);
        }
    });
    return pacientesFilter;
}

// se encarga de buscar por un termino dentro de un arreglo de objetos

export function buscarPacienteTxt( pacientes:any[] , busqueda:any ) {
    const pacientesFilter:any[] = [];
    pacientes.forEach((element:any) => {
        if(element.nombrePaciente.includes(busqueda.toUpperCase())){
            pacientesFilter.push(element);
        }
    });
    return pacientesFilter;
}
