import  {  RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './componets/dashboard/dashboard/dashboard.component';
import { LoginComponent } from './login/login/login.component';
import { PageComponent } from './pages/page/page.component';



const appRoutes = [
    { path:"", component: PageComponent, children: [

        {path: 'dashborad', component: DashboardComponent },
        {path: '', redirectTo:'/dashboard', pathMatch:"full" },
    ]},
    { path: 'login', component:LoginComponent  },
    { path: '**', component: LoginComponent }
];


export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash:true, relativeLinkResolution: 'legacy'});