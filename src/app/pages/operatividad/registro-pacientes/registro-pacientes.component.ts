import { Component, OnInit, ViewChild } from '@angular/core';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import Swal from 'sweetalert2';
import {  Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-registro-pacientes',
  templateUrl: './registro-pacientes.component.html',
  styleUrls: ['./registro-pacientes.component.css']
})
export class RegistroPacientesComponent implements OnInit {
  registroNgForm: FormGroup = new FormGroup ({
    nombrePaciente: new FormControl(''),
    apellidoPaterno: new FormControl(''),
    apellidoMaterno: new FormControl(''),
    fechaNacimientoPaciente: new FormControl(''),
    edad: new FormControl(''),
    genero : new FormControl('')
  });

  registroForm!: FormGroup;

  constructor( public _pacienteService : PacientesService,
               private _formBuilder: FormBuilder,
               private _router: Router,
               private spinner: NgxSpinnerService
    ) { }

  ngOnInit(): void {
    this.registroForm = this._formBuilder.group({
      nombrePaciente              : ['', [Validators.required]],
      apellidoPaterno             : ['', [Validators.required]],
      apellidoMaterno             : [''],
      fechaNacimientoPaciente     : ['', [Validators.required]],
      edad                        : ['0'],
      genero                      : ['', [Validators.required]],
    })
    
  }

  nuevoDisponente(){
    console.log(this.registroForm.value);
    
    if( this.registroForm.valid ) {
      this.spinner.show();
      this._pacienteService.setPacientes(this.registroForm.value)
      .subscribe((data:any) => {
           if(  data['ok'] ){
             this.spinner.hide();
             Swal.fire({
                 icon: 'success',
                 title: '',
                 text: 'PACIENTE REGISTRADO',
               })
             this._router.navigateByUrl('/bitacora/disponentes')
           }
        }, 
        error => {
          console.log(error)
          Swal.fire({
            icon: 'error',
            title:  error.error.message.toUpperCase(),
            text: '',
          })
        }
        );
    }
  }

}
