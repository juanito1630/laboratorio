import { Component, OnInit } from '@angular/core';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

//functions
import { buscarPacienteTxt } from '../../../helpers/filters';
@Component({
  selector: 'app-lista-pacientes',
  templateUrl: './lista-pacientes.component.html',
  styleUrls: ['./lista-pacientes.component.css']
})
export class ListaPacientesComponent implements OnInit {

  public pagina = 0;
  public pacientes:any = [];
  public listaEstudio:any;
  public numero = 6;
  public nToString:any;
  public listPacientes:any[] = [];
  public txtSearchPaciente = "";

  constructor(private _nPacientes: PacientesService) { }

  ngOnInit(): void {
    this.obtenerPacientes();
  }


  obtenerPacientes(){
    this._nPacientes.getPacientes()
    .subscribe((data:any) => {
      // if(data.ok) {
        this.pacientes = data.data
        this.listaEstudio =data.data.results;
        this.listPacientes = data.data;
    });
  }

  filterPacientes() {

    console.log( this.pacientes);
    this.pacientes = this.listPacientes;
    if( this.txtSearchPaciente.length > 2 ){
      const pacientesFilter =  buscarPacienteTxt(this.pacientes, this.txtSearchPaciente);
      this.pacientes = pacientesFilter;
    }

  }

}
