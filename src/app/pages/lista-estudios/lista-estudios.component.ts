import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NuevosEstudiosService } from 'src/app/services/estudios/nuevos-estudios.service';
import { buscarPaciente } from '../../helpers/filters'
@Component({
  selector: 'app-lista-estudios',
  templateUrl: './lista-estudios.component.html',
  styleUrls: ['./lista-estudios.component.css']
})
export class ListaEstudiosComponent implements OnInit {

  public serviceSi = [1,1,1,1]
  public estudios:any[] = [];
  public pagina = 0;
  public totPages = 5;
  public listaEstudio:any;
  public txtBuscarEstudios = "";
  public estudiosEncontrados:any[] = [];

  constructor(
    private _nEstudios: NuevosEstudiosService
  ) { }

  ngOnInit(): void {
    this.obtenerEstudios()
  }

  obtenerEstudios(){
    this._nEstudios.getEstudios()
    .subscribe((data:any) => {
      console.log(data);
      // if(data.ok) {
        this.estudios = data.data
        this.estudiosEncontrados = data.data;
    });
  }

  buscarEstudio(){

    this.estudios = this.estudiosEncontrados;
    let arrFilter:any[];

    if( this.txtBuscarEstudios.length > 2 ){
    
      arrFilter= buscarPaciente( this.estudios, this.txtBuscarEstudios ); 
      this.estudios = arrFilter;

    }

  }

}
