import { Routes, RouterModule } from '@angular/router';
import { EstudiosComponent } from './estudios/estudios/estudios.component';
import { ListaEstudiosComponent } from './lista-estudios/lista-estudios.component';
import { ListaPacientesComponent } from './operatividad/lista-pacientes/lista-pacientes.component';
import { RegistroPacientesComponent } from './operatividad/registro-pacientes/registro-pacientes.component';
import { PageComponent } from './page/page.component';

const pageRoutes: Routes = [{
    path:"", component: PageComponent, children: [
        //estudios
        { path:"nuevos/estudios", component: EstudiosComponent},
        {path:"listado/estudios", component: ListaEstudiosComponent },
        // pacientes
        { path:"registro/pacientes", component: RegistroPacientesComponent },
        { path: "listado/pacientes", component: ListaPacientesComponent },
        { path:"listado/estudios", component: ListaEstudiosComponent },
        { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    ]
}];


export const PAGES_ROUTES = RouterModule.forChild(pageRoutes);
