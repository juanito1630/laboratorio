import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {  FormBuilder, FormGroup, FormControlName, Validators  } from '@angular/forms';
import { NuevosEstudiosService } from 'src/app/services/estudios/nuevos-estudios.service';

@Component({
  selector: 'app-estudios',
  templateUrl: './estudios.component.html',
  styleUrls: ['./estudios.component.css']
})
export class EstudiosComponent implements OnInit {

  public forma:any;

  constructor(private _fb: FormBuilder, private _router:Router, private _route:ActivatedRoute, private _nEstudios: NuevosEstudiosService) {  
   }

  ngOnInit(){

    this.forma = this._fb.group({
      ESTUDIO:['',[Validators.required, Validators.minLength(3) ]],
      PRECIO_PUBLICO:['', [ Validators.min(0)]],
    });
  }

  guardar(){

      console.log(this.forma.value);
      
    this._nEstudios.enviarEstudiosNuevos( this.forma.value).subscribe( (data:any) => {
      // if(data.ok){
      //   console.log(this.forma);
      //   console.log("Se creo el servicio", "Servicio creado", "success");
      //   // this._router.navigateByUrl('serviciosInt/'+this.id);
      // }
      console.log(data);
      
    })
   
  }

}