import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page/page.component';
import { PAGES_ROUTES } from './page.routes';
import { EstudiosComponent } from './estudios/estudios/estudios.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../componets/components.module';
import { RegistroPacientesComponent } from './operatividad/registro-pacientes/registro-pacientes.component';
import { ListaEstudiosComponent } from './lista-estudios/lista-estudios.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListaPacientesComponent } from './operatividad/lista-pacientes/lista-pacientes.component';


@NgModule({
  declarations: [
    PageComponent,
    EstudiosComponent,
    RegistroPacientesComponent,
    ListaEstudiosComponent,
    ListaPacientesComponent
  ],

  exports: [
    ReactiveFormsModule,
  ], schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  imports: [
    CommonModule,
    PAGES_ROUTES,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ]
})
export class PageModule { }
